<?php

namespace App\Tests\Unit;

use App\Services\DataInput\XmlInputService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class XmlInputServiceTest extends TestCase
{
    private XmlInputService $xmlInputService;

    protected function setUp(): void
    {
        parent::setUp();

        $loggerMock = $this->createMock(LoggerInterface::class);

        $this->xmlInputService = new XmlInputService($loggerMock);
    }

    /**
     * @test
     * @dataProvider readDataFailureCases
     */
    public function readDataFailure(string $path): void
    {
        // TODO: for more precise testing check additionally if correct exception was thrown

        $this->assertSame(
            false,
            $this->xmlInputService->readData($path)->readDataSuccessfully
        );
    }

    /** @test */
    public function readDataSuccessOneItem(): void
    {
        $path = __DIR__ . '/../Data/oneItemFile.xml';
        $readDataResult = $this->xmlInputService->readData($path);

        $this->assertSame(
            true,
            $readDataResult->readDataSuccessfully
        );
        $this->assertSame(
            1,
            sizeof($readDataResult->results)
        );
    }

    /** @test */
    public function readDataSuccessMultipleItems(): void
    {
        $path = __DIR__ . '/../Data/twoItemsFile.xml';
        $readDataResult = $this->xmlInputService->readData($path);

        $this->assertSame(
            true,
            $readDataResult->readDataSuccessfully
        );
        $this->assertSame(
            2,
            sizeof($readDataResult->results)
        );
    }

    private function readDataFailureCases(): array
    {
        return [
            [''],
            ['FileDoesNotExist'],
            [__DIR__ . '/../Data/emptyFile.xml'],
            [__DIR__ . '/../Data/uriFile.xml'],
            [__DIR__ . '/../Data/oneIncorrectItemFile.xml'],
        ];
    }
}