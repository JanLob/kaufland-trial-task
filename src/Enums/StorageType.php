<?php

declare(strict_types=1);

namespace App\Enums;

enum StorageType: int
{
    case WriteToMysql = 1;
    case WriteToMongodb = 2;
}
