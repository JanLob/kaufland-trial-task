<?php

declare(strict_types=1);

namespace App\Enums;

enum InputType: int
{
    case ReadFromXml = 1;
    case ReadFromJson = 2;
}
