<?php

declare(strict_types=1);

namespace App\DataObjects;

use App\Entity\Coffee;
use Exception;

class ReadDataResult
{
    public function __construct(
        public readonly bool       $readDataSuccessfully,
        public readonly ?Exception $exception = null,
        /** @var Coffee[] */
        public readonly ?array     $results = null,
    )
    {
    }
}