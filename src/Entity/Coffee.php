<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Coffee
{
    // Using generated id in addition to the given entity_id, since there will potentially be other data sources.
    // Also make all data fields nullable because of that.
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function __construct(
        #[ORM\Column(nullable: true)]
        private ?int    $entityId = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $categoryName = null,

        #[ORM\Column(nullable: true)]
        private ?string $sku = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $name = null,

        #[ORM\Column(length: 511, nullable: true)]
        private ?string $description = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $shortdesc = null,

        #[ORM\Column(nullable: true)]
        private ?float  $price = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $link = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $image = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $brand = null,

        #[ORM\Column(nullable: true)]
        private ?int    $rating = null,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $caffeineType = null,

        #[ORM\Column(nullable: true)]
        private ?int    $count = null,

        #[ORM\Column(nullable: true)]
        private ?bool   $flavored = null,

        #[ORM\Column(nullable: true)]
        private ?bool   $seasonal = null,

        #[ORM\Column(nullable: true)]
        private ?bool   $inStock = null,

        #[ORM\Column(nullable: true)]
        private ?bool   $facebook = null,

        #[ORM\Column(nullable: true)]
        private ?bool   $isKCup = null,
    )
    {
    }
}
