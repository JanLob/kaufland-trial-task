<?php

declare(strict_types=1);

namespace App\Utils;

class Utils
{
    static public function cutString255(string $string): string
    {
        if (strlen($string) > 255) {
            return substr($string, 0, 255);
        } else {
            return $string;
        }
    }
}