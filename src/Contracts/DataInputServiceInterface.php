<?php

declare(strict_types=1);

namespace App\Contracts;

use App\DataObjects\ReadDataResult;

interface DataInputServiceInterface
{
    public function readData(string $path): ReadDataResult;
}