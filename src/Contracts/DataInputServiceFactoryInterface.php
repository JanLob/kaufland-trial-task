<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Enums\InputType;

interface DataInputServiceFactoryInterface
{
    public function create(InputType $inputType): DataInputServiceInterface;
}