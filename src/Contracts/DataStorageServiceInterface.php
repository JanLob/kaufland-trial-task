<?php

declare(strict_types=1);

namespace App\Contracts;

use App\DataObjects\ReadDataResult;

interface DataStorageServiceInterface
{
    public function writeData(ReadDataResult $readDataResult): void;
}