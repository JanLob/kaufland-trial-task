<?php

declare(strict_types=1);

namespace App\Services\DataStorage;

use App\Contracts\DataStorageServiceFactoryInterface;
use App\Contracts\DataStorageServiceInterface;
use App\Enums\StorageType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class DataStorageServiceFactory implements DataStorageServiceFactoryInterface
{
    public function __construct(
        private readonly LoggerInterface        $logger,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @throws Exception
     */
    final public function create(StorageType $storageType): DataStorageServiceInterface
    {
        return match ($storageType) {
            StorageType::WriteToMysql => new MySqlStorageService($this->logger, $this->entityManager),
            StorageType::WriteToMongodb => new MongoDbStorageService(),
        };
    }
}