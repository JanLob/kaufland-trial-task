<?php

declare(strict_types=1);

namespace App\Services\DataStorage;

use App\Contracts\DataStorageServiceInterface;
use App\DataObjects\ReadDataResult;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class MySqlStorageService implements DataStorageServiceInterface
{
    public function __construct(
        private readonly LoggerInterface        $logger,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    final public function writeData(ReadDataResult $readDataResult): void
    {
        foreach ($readDataResult->results as $coffee) {
            try {
                $this->entityManager->persist($coffee);
            } catch (Exception $exception) {
                $this->logger->error($exception);
            }
        }

        $this->entityManager->flush();
    }
}