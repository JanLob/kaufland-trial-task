<?php

declare(strict_types=1);

namespace App\Services\DataInput;

use App\Contracts\DataInputServiceInterface;
use App\DataObjects\ReadDataResult;
use App\Exceptions\NotImplementedException;

class JsonInputService implements DataInputServiceInterface
{
    final public function readData(string $path): ReadDataResult
    {
        throw new NotImplementedException();
    }
}