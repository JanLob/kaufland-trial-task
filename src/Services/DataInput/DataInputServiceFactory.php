<?php

declare(strict_types=1);

namespace App\Services\DataInput;

use App\Contracts\DataInputServiceFactoryInterface;
use App\Contracts\DataInputServiceInterface;
use App\Enums\InputType;
use Exception;
use Psr\Log\LoggerInterface;

class DataInputServiceFactory implements DataInputServiceFactoryInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
    )
    {
    }

    /**
     * @throws Exception
     */
    final public function create(InputType $inputType): DataInputServiceInterface
    {
        return match ($inputType) {
            InputType::ReadFromXml => new XmlInputService($this->logger),
            InputType::ReadFromJson => new JsonInputService(),
        };
    }
}