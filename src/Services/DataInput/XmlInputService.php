<?php

namespace App\Services\DataInput;

use App\Contracts\DataInputServiceInterface;
use App\DataObjects\ReadDataResult;
use App\Entity\Coffee;
use App\Utils\Utils;
use Exception;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;

class XmlInputService implements DataInputServiceInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
    )
    {
    }

    /**
     * @throws Exception
     */
    final public function readData(string $path): ReadDataResult
    {
        try {
            $simpleXmlElement = simplexml_load_file(
                $path,
                'SimpleXmlElement',
                LIBXML_NOCDATA
            );
        } catch (Exception $exception) {
            $this->logger->error($exception);
            return new ReadDataResult(false, exception: $exception);
        }

        if ($simpleXmlElement === false) {
            $this->logger->error('Could not read xml file.');
            return new ReadDataResult(false, exception: new Exception('Could not read xml file.')
            );
        }

        try {
            $items = get_object_vars($simpleXmlElement)['item'];
        } catch (Exception $exception) {
            $this->logger->error($exception);
            return new ReadDataResult(false, exception: $exception);
        }

        if (is_array($items)) {
            try {
                $coffeeEntities = array_map([$this, 'parseCoffee'], $items);
            } catch (Exception $exception) {
                return new ReadDataResult(false, exception: $exception);
            }
            return new ReadDataResult(true, results: $coffeeEntities);
        }

        try {
            $coffeeEntities = [$this->parseCoffee($items)];
        } catch (Exception $exception) {
            return new ReadDataResult(false, exception: $exception);
        }

        return new ReadDataResult(true, results: $coffeeEntities);
    }

    private function parseCoffee(SimpleXMLElement $coffeeItem): Coffee
    {
        $coffeeArray = get_object_vars($coffeeItem);

        $description = $this->parseXmlDescription($coffeeArray['description']);

        $flavored = strtolower($coffeeArray['Flavored']);
        $seasonal = strtolower($coffeeArray['Seasonal']);
        $inStock = strtolower($coffeeArray['Instock']);

        return new Coffee(
            (int)$coffeeArray['entity_id'],
            Utils::cutString255($coffeeArray['CategoryName']),
            Utils::cutString255($coffeeArray['sku']),
            Utils::cutString255($coffeeArray['name']),
            $description,
            Utils::cutString255($coffeeArray['shortdesc']),
            (float)$coffeeArray['price'],
            Utils::cutString255($coffeeArray['link']),
            Utils::cutString255($coffeeArray['image']),
            Utils::cutString255($coffeeArray['Brand']),
            (int)$coffeeArray['Rating'],
            Utils::cutString255($coffeeArray['CaffeineType']),
            (int)$coffeeArray['Count'],
            $flavored == 'yes' ? true : ($flavored == 'no' ? false : null),
            $seasonal == 'yes' ? true : ($seasonal == 'no' ? false : null),
            $inStock == 'yes' ? true : ($inStock == 'no' ? false : null),
            (bool)$coffeeArray['Facebook'],
            (bool)$coffeeArray['IsKCup'],
        );
    }

    private function parseXmlDescription(mixed $description): ?string
    {
        if ($description instanceof SimpleXMLElement) {
            $descriptionArray = get_object_vars($description);
            if (empty($descriptionArray)) {
                return null;
            } elseif ($descriptionArray[0] instanceof string) {
                return $descriptionArray[0];
            } else {
                return null;
            }
        } elseif ($description instanceof string) {
            return Utils::cutString255($description);
        } else {
            return null;
        }
    }
}