<?php

namespace App\Commands;

use App\Contracts\DataInputServiceFactoryInterface;
use App\Contracts\DataStorageServiceFactoryInterface;
use App\Enums\InputType;
use App\Enums\StorageType;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'kaufland:data', description: 'Read data from a given source and store it into the data base.')]
class DataCommand extends Command
{
    const PATH_TO_XML_FILE = __DIR__ . '/../../data/feed.xml';

    public function __construct(
        private readonly DataInputServiceFactoryInterface   $dataInputServiceFactory,
        private readonly DataStorageServiceFactoryInterface $dataStorageServiceFactory,
        string                                              $name = null,
    )
    {
        parent::__construct($name);
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0 if everything went fine, or an exit code.
     * @throws Exception
     */
    final public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $dataInputType = (int)$io->ask("Please specify the data source? \n [1] => xml\n [2] => json");
        $dataStorageType = (int)$io->ask("Please specify the  data storage? \n [1] => mysql\n [2] => mongodb");

        $dataInputType = InputType::tryFrom($dataInputType);
        $dataStorageType = StorageType::tryFrom($dataStorageType);

        if (is_null($dataInputType) || is_null($dataStorageType)) {
            if ($io->confirm('Invalid values, want to try again?')) {
                return $this->execute($input, $output);
            } else {
                return Command::INVALID;
            }
        }

        $readDataResult = $this->dataInputServiceFactory->create($dataInputType)->readData(self::PATH_TO_XML_FILE);

        if ($readDataResult->readDataSuccessfully) {
            $this->dataStorageServiceFactory->create($dataStorageType)->writeData($readDataResult);
        } else {
            $io->info('Could not read data. Pleas try again.');
            return $this->execute($input, $output);
        }

        if ($io->confirm("Successfully stored data in data base.\n Do you want to store more data?")) {
            return $this->execute($input, $output);
        }

        return Command::SUCCESS;
    }
}