<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240208102355 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE coffee (
                id INT AUTO_INCREMENT NOT NULL, 
                entity_id INT DEFAULT NULL, 
                category_name VARCHAR(255) DEFAULT NULL, 
                sku VARCHAR(255) DEFAULT NULL, 
                name VARCHAR(255) DEFAULT NULL, 
                description VARCHAR(511) DEFAULT NULL, 
                shortdesc VARCHAR(255) DEFAULT NULL, 
                price DOUBLE PRECISION DEFAULT NULL, 
                link VARCHAR(255) DEFAULT NULL, 
                image VARCHAR(255) DEFAULT NULL, 
                brand VARCHAR(255) DEFAULT NULL, 
                rating INT DEFAULT NULL, 
                caffeine_type VARCHAR(255) DEFAULT NULL, 
                count INT DEFAULT NULL, 
                flavored TINYINT(1) DEFAULT NULL, 
                seasonal TINYINT(1) DEFAULT NULL, 
                in_stock TINYINT(1) DEFAULT NULL, 
                facebook TINYINT(1) DEFAULT NULL, 
                is_kcup TINYINT(1) DEFAULT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4'
        );
    }
}
